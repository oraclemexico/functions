 import fn = require("oci-functions");
 import core = require("oci-core");
 import identity = require("oci-identity");
 import common = require("oci-common");
 
 const provider: common.ConfigFileAuthenticationDetailsProvider = new common.ConfigFileAuthenticationDetailsProvider();
 
 const INVOKE: string = "invoke";
 
 const args = process.argv.slice(2);
 // Handle commands: {setup, invoke, teardown}
 let commands = new Set();
 commands.add(args[0]);
 commands.add(args[1]);
 commands.add(args[2]);
 
 // All resources will be prefixed with this name.
 const name: string = process.env.FUNCTION_ASSET_NAME ? process.env.FUNCTION_ASSET_NAME : "FunctionsApplication";
 
 // We need a target compartment.
 const compartmentId = process.env.COMPARTMENT_ID;
 
 // We need an accessible image in the region to invoke.
 // e.g. phx.ocir.io/tenancy-name/registry/imagename:version
 const image = process.env.OCIR_FN_IMAGE;
 
 if (!compartmentId) {
   console.log("Please set the mandatory environment variables - COMPARTMENT_ID, OCIR_FN_IMAGE");
   process.exit(-1);
 }
 
 // Depending on the image chosen a payload can be specified.
 const payload: string = process.env.FN_PAYLOAD ? process.env.FN_PAYLOAD : "";
 
 // create Client
 const identityClient: identity.IdentityClient = new identity.IdentityClient({
   authenticationDetailsProvider: provider
 });
 const fnManagementClient: fn.FunctionsManagementClient = new fn.FunctionsManagementClient({
   authenticationDetailsProvider: provider
 });
 const vcnClient: core.VirtualNetworkClient = new core.VirtualNetworkClient({
   authenticationDetailsProvider: provider
 });
 const fnInvokeClient: fn.FunctionsInvokeClient = new fn.FunctionsInvokeClient({
   authenticationDetailsProvider: provider
 });
 
 (async () => {
   try {
     if (commands.has(INVOKE)) {
       await invokeFunction(compartmentId!, name, payload);
     } else {
       console.log("Unknown command");
     }
   } catch (error) {
     console.log("Not able to run Invoke function example . Error" + error);
   }
 })();
 

 /**
  * Create all the OCI and Fn resources required to invoke a function.
  * @param compartmentId the compartment in which to created the required
  *                      resources.
  * @param name          a name prefix to easilly identifty the resources.
  * @param image         a valid OCIR image for the function.
  */
 async function invokeFunction(compartmentId: string, name: string, payload: string) {
   try {
     // invoke function
     const appName: string = applicationName(name);
     const fnName = functionName(name);
 
     const fnSummary: fn.models.FunctionSummary = await getUniqueFunctionByName(
       fnManagementClient,
       compartmentId,
       appName,
       fnName
     );

     console.log("Function summary ", fnSummary);
     const response = await invokeFunctionHelper(fnInvokeClient, fnSummary, payload);
     // invokeFunction returns a readableStream. parse the stream to view payload.
     console.log(await common.getStringFromResponseBody(response.value));
   } catch (error) {
     console.log("Error invoking function " + error);
   }
 }
 
 
 /**
  * Gets Function information. This is an expensive operation and the results should be cached.
  *
  * @param fnManagementClient the service client to use to get the Function information.
  * @param compartmentId of the application and function.
  * @param applicationDisplayName of the application.
  * @param functionDisplayName of the function.
  * @return the FunctionSummary.
  */
 async function getUniqueFunctionByName(
   client: fn.FunctionsManagementClient,
   compartmentId: string,
   applicationDisplayName: string,
   functionDisplayName: string
 ): Promise<fn.models.FunctionSummary> {
   const application: fn.models.ApplicationSummary = await getUniqueApplicationByName(
     fnManagementClient,
     compartmentId,
     applicationDisplayName
   );
   const fn = await getUniqueFunction(fnManagementClient, application.id!, functionDisplayName);
   return fn;
 }
 
 /**
  * Gets the Application info of a single uniquely named Application in the specified compartment.
  *
  * @param fnManagementClient the service client to use to get the Application information.
  * @param compartmentId of the application.
  * @param applicationDisplayName of the application.
  * @return the ApplicationSummary.
  */
 async function getUniqueApplicationByName(
   client: fn.FunctionsManagementClient,
   compartmentId: string,
   applicationDisplayName: string
 ): Promise<fn.models.ApplicationSummary> {
   //Find the application in a specific compartment
   const listAppRequest: fn.requests.ListApplicationsRequest = {
     displayName: applicationDisplayName,
     compartmentId: compartmentId
   };
   const listAppResponse = await client.listApplications(listAppRequest);
   if (listAppResponse.items.length !== 1) {
     throw "Could not find unique application with name " +
       applicationDisplayName +
       " in compartment " +
       compartmentId;
   }
   return listAppResponse.items[0];
 }
 
 /**
  * Gets Function information. This is an expensive operation and the results should be cached.
  *
  * @param fnManagementClient the service client to use to get the Function information.
  * @param applicationId of the function to find.
  * @param functionDisplayName of the function to find.
  * @return the FunctionSummary.
  * @throws Exception
  */
 async function getUniqueFunction(
   client: fn.FunctionsManagementClient,
   applicationId: string,
   functionDisplayName: string
 ): Promise<fn.models.FunctionSummary> {
   const listFnRequest: fn.requests.ListFunctionsRequest = {
     applicationId: applicationId,
     displayName: functionDisplayName
   };
   const listFnResponse = await client.listFunctions(listFnRequest);
   if (listFnResponse.items.length !== 1) {
     throw "Could not find function with name " +
       functionDisplayName +
       " in application " +
       applicationId;
   }
   return listFnResponse.items[0];
 }
 
 /**
  * Invokes a function.
  *
  * @param fnInvokedClient the service client to use to delete the Function.
  * @param function the Function to invoke.
  * @param payload the payload to pass to the function.
  *
  * @throws Exception if there is an error when invoking the function.
  */
 async function invokeFunctionHelper(
   client: fn.FunctionsInvokeClient,
   fnSummary: fn.models.FunctionSummary,
   payload: string
 ): Promise<fn.responses.InvokeFunctionResponse> {
   console.log("Invoking function endpoint - " + fnSummary.invokeEndpoint);
 
   // Configure the client to use the assigned function endpoint.
   client.endpoint = fnSummary.invokeEndpoint!;
   const functionId = fnSummary.id || "";
 
   const request: fn.requests.InvokeFunctionRequest = {
     functionId: functionId
   };
   const response = await client.invokeFunction(request);
   return response;
 }
 
 
 function applicationName(name: string) {
   return name;
 }
 
 function functionName(name: string) {
   return process.env.FUNCTION_NAME ? process.env.FUNCTION_NAME : name;
 }
