# Oracle Functions #

This projects pretends to documentate the non-scripted walk thru deploying and consuming a function

## Prereqs

* docker

### OCI related config

* ##### VCN Creation

Go to Virtual Cloud Network and click on "Start VCN Wizard" button.

Once the wizard is shown, select the "Create VCN Internet Connectivity" and click "Start VCN Wizard" button.

![Function Application Creation](assets/asset1.png)

Provide the VCN Name and click "Next" button

![Function Application Creation](assets/asset2.png)

Confirm the data clicking on "Create" button

* ##### API Key creation and OCI configuration file

In order to interact with the remote function server, we need to create a config file to allow the fn cli to know how to interact with the remote server.

To do that, click in the "user" icon in the right top corner

![Function Application Creation](assets/asset5.png)

Go to "User settings"

![Function Application Creation](assets/asset6.png)

Then click in the "API Keys" option in the left side menu and click in the "Add API Key"

![Create API Key](assets/asset7.png)

Select the "Generate API Key Pair" option and download the public and private key, and click on "Add" button

![Create API Key](assets/asset8.png)

Copy the configuraion file Preview content and save in your local machine in a file called config.

This configuration file snippet includes the basic authentication information you’ll need to use the SDK, CLI, or other OCI developer tool. Paste the contents of the text box into your ~/.oci/config file and update the key_file parameter with the file path to your private key.

> On Windows, you can use PowerShell to create the folder with the following command: 
> File Explorer does not support creating folder names that start with a period.

```bash
mkdir %HOMEDRIVE%%HOMEPATH%\.oci. 
```
It is needed to update the location of the recently downloaded key as follows:

```bash
key_file=/Users/maleficarum/.oci/oracleidentitycloudservice_oscar.ventura-09-21-21-18.pem
```

* ##### Function application creation

Once the VCN is created, go to Functions feature on the cloud dashboard or search "Function" in the search box, and click in "Applications".

![Function Application Creation](assets/asset3.png)

Once in the Applications section, click in the "Create Application" button to create the Function Application we will be working on.

Provide the Application Name, select the recently created VCN and add the networks where the functions will be deployed on, and click on "Create".

![Function Application Creation](assets/asset4.png)

$ fn init --runtime [go|java|node|python] firstFunction

### Functions setup

We need to install de fn cli on the local machine in order to manage the created funcions.

* Fn CLI installation

```bash
$ curl -LSs https://raw.githubusercontent.com/fnproject/cli/master/install | sh
```

Once installed, check the environment

```bash
$ fn version
Client version is latest version: 0.6.8
Server version:  ?
```

Now, set the functions context to work with the remote server (the OCI instances) and create the basic function

```bash
#Init the function with the proper langage
$ fn init --runtime [go|java|node|python] my-func

#change to the created function directory structure
$ cd my-func

#Set the remote context as 'Oracle provider'
$ fn create context [ContextName] --provider oracle

#set the compartment, registry and API Url of the remote context
$ fn update context oracle.compartment-id [CompartmentID]
$ fn update context api-url [API_URL]

#The repo name MUST BE LOWERCASE
$ fn update context registry iad.ocir.io/idcuofyoqrjp/[OCIR-REPO]

#Login to the remote docker registry to push the created functions
$ docker login -u 'idcuofyoqrjp/oracleidentitycloudservice/[OCI_USERNAME]' iad.ocir.io

#Set the default current context to the recently created
$ fn use context [ContextName]

#Delply and invoke the created function
$ fn deploy --app FunctionsApplication
$ fn invoke FunctionsApplication firstfunction
```

#### Docker login notes

docker login <region-key>.ocir.io

Where region key is located at https://docs.oracle.com/en-us/iaas/Content/Registry/Concepts/registryprerequisites.htm#regional-availability

When prompted for Username, enter the name of the user you will be using with Oracle Functions to create and deploy functions, in the format:

```
<tenancy-namespace>/<username>
```

where <tenancy-namespace> is the auto-generated Object Storage namespace string of the tenancy in which to create repositories (as shown on the Tenancy Information page). For example, ansh81vru1zp/jdoe@acme.com.

Note that for some older tenancies, the namespace string might be the same as the tenancy name in all lower-case letters (for example, acme-dev).

If your tenancy is federated with Oracle Identity Cloud Service, use the format 

```
<tenancy-namespace>/oracleidentitycloudservice/<username>.
```

The tenancy name space is located at the tenancy details, clicking in the Profile icon -> Tenancy: <tenancy name> under Object Storage Settings

When prompted for Password, enter the user's Oracle Cloud Infrastructure auth token; to fetch Auth Token, go to "Profile icon", and click in the "User Settings" option, and click in the "Auth Token" menu to create the token. This token is available to copy once so copy and save it in a safe place.


### Consuming Oracle Fn from code

To invoke the remote function we have to install TypeScript "compiler" locally:

```bash
$ npm install -g typescript
```

Now we can compile invokeFunction.ts

```bash


$ export OCIR_FN_IMAGE=iad.ocir.io/idcuofyoqrjp/[CONTAINER_IMAGE_REGISTRY]/firstfunction:0.0.2
$ export COMPARTMENT_ID=[COMPARTMENT_ID]
$ export FUNCTION_ASSET_NAME=FunctionsProject
$ export FUNCTION_NAME=firstfunction

$ tsc invokeFunction.ts && node invokeFunction.js invoke
```

### Adding dependencies to FN function

In the package.json add the next dependency to looks like the next snippet.

```json
{
        "name": "hellofn",
        "version": "1.0.0",
        "description": "example function",
        "main": "func.js",
        "author": "",
        "license": "Apache-2.0",
        "dependencies": {
                "@fnproject/fdk": ">=0.0.26",
                "node-fetch": "^2.6.1"
        }
}
```
